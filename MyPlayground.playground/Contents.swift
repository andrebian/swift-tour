//: Playground - noun: a place where people can play

// Exemplos seguidos de (https://developer.apple.com/library/prerelease/ios/documentation/Swift/Conceptual/Swift_Programming_Language/GuidedTour.html#//apple_ref/doc/uid/TP40014097-CH2-ID1)

import UIKit

print("Hello World")



var myVariable = 42
myVariable = 50
let myConstant = 42




let implicitInteger = 70
let implicitDouble = 70.0
let explicitDouble: Double = 70




let label = "The width is "
let width = 94
let widthLabel = label + String(width)




let apples = 3
let oranges = 5
let appleSummary = "I have \(apples) apples."
let fruitSummary = "I have \(apples + oranges) pieces of fruit."




// Array Creation

var shoppingList = ["catfish", "water", "tulips", "blue paint"]
shoppingList[1] = "bottle of water"

var occupations = [
    "Andre": "Developer",
    "Francis": "Developer"
]

occupations["Luís"] = "Sales"

print(occupations)


// Empty array

let emptyArray = [String()]
var emptyDictionary = [String: Float]()


// Control Flow

let individualScores = [75, 43, 103, 87, 12]
var teamScore = 0

for score in individualScores {
    
    // No exemplo padrão foi utilizado um else, no entanto, por boas práticas, eu removi o mesmo!
    // Ambos dão o mesmo resultado
    
    var temporaryScore = 1
    if score > 50 {
        temporaryScore = 3
    }
    
    teamScore += temporaryScore
}

print(teamScore)




// Optionals

var optionalString: String? = "Hello"
print(optionalString == nil)

var optionalName: String? = "John Appleseed"
var greeting = "Hello!"

if let name = optionalName {
    greeting = "Hello, \(name)"
}


let nickName: String? = nil
let fullName: String = "John Appleseed"
let informalGreeting = "Hi \(nickName ?? fullName)"




let vegetable = "red pepper"

switch vegetable {
    
    case "celery":
        print("Add slome raisins and make ants on a log")
    
    case "cucumber", "watercress":
        print("That would make a good tea sandwich")
    
    case let x where x.hasSuffix("pepper") :
        print("Is it a spicy \(x)?")
    
    default:
        print("Everything tastes good in a soup.")
    
}



let interestingNumbers = [
    "Prime": [2, 3, 5, 7, 11, 13],
    "Fibonacci": [1, 1, 2, 3, 5, 8, 13, 21, 44],
    "Square": [1, 4, 9, 16, 25]
]

var largest = 0

for (kind, numbers) in interestingNumbers {
    for number in numbers {
        if number > largest {
            largest = number
        }
    }
}
print(largest)

var teste = 0
for teste; teste < 5; teste++ {
    print(teste)
}


// Loop repetições

var n = 2
while n < 200 {
    n = n * 2
}
print(n)

var m = 2
repeat {
    m = m * 2
} while m < 100
print(m)


var firstForLoop = 0
for i in 0..<16 {
    firstForLoop += i
}
print(firstForLoop)


var secondForLoop = 0
for var i = 0; i < 4; ++i {
    secondForLoop += i
}
print(secondForLoop)



// Functions and closures



func greet(name: String, day: String, year: String) -> String {
    return "Hello \(name), today is \(day as NSString) in \(year)."
}
greet("Bob", day: "Tuesday", year: "2015")


// Multiple return
func calculateStatistics(scores: [Int]) -> (min: Int, max: Int, sum: Int) {
    
    var min = scores[0]
    var max = scores[0]
    var sum = 0
    
    
    for score in scores {
        if score > max {
            max = score
        } else if score < min {
            min = score
        }
        sum += score
    }
    
    return(min, max, sum)
}

// Usage for Multiple return function
let statistics = calculateStatistics([5, 3, 100, 3, 9])
print(statistics.min)
print(statistics.max)
print(statistics.sum)
print(statistics.2)



// Variadic Function

// Soma de números
func sumOf(numbers: Int...) -> Int {
    var sum = 0
    for number in numbers {
        sum += number
    }
    
    return sum
}

sumOf()
sumOf(42, 597, 12)


// Média
func averageOf(numbers: Int...) -> Int {
    
    var total = 0
    var totalItems = 0
    var average = 0
    
    for number in numbers {
        total += number
        totalItems++
    }
    
    if totalItems > 0 {
        average = (total / totalItems)
    }
    
    return average
}

averageOf(35, 89, 158)
averageOf()



// Nested function

func returnFifteen() -> Int {
    var y = 10
    func add() {
        y += 5
    }
    
    add()
    return y
}

returnFifteen()


func makeIncrementer() ->((Int) -> Int) {
    func addOne(number: Int) -> Int {
        return 1 + number
    }
    
    return addOne
}

var increment = makeIncrementer()
increment(7)


// Function calling another function in parameter
func hasAnyMatches(list: [Int], condition: (Int) -> Bool) ->Bool {
    for item in list {
        if condition(item) {
            return true
        }
    }
    return false
}

func lessThanTen(number: Int) -> Bool {
    return number < 10
}

func moreThanTen(number: Int) -> Bool {
    return number > 10
}

var numbers = [20, 19, 34, 12]
hasAnyMatches(numbers, condition: moreThanTen)


// Closure

numbers.map({
    (number: Int) -> Int in
    let result = 3 * number
    return number
})

let mappedNumbers = numbers.map({number in 3 * number})
print(mappedNumbers)

let sortedNumbers = numbers.sort { $1 > $0 }
print(sortedNumbers)



// Objects and classes

class Shape {
    
    
    var numberOfSides: Int = 0
    
    func simpleDescription() -> String {
        return "A shape with \(numberOfSides) sides."
    }
    
    func addSide(sides: Int) -> Void {
        if sides > 0 {
            numberOfSides += sides
        }
    }
    
    func removeSide(side: Int) -> Void {
        if numberOfSides > 0 && side > 0 {
            numberOfSides -= side
        }
    }
    
}

let shape = Shape()
print(shape.simpleDescription())

shape.addSide(3)
print(shape.simpleDescription())

shape.removeSide(1)
print(shape.simpleDescription())

shape.numberOfSides = 18
print(shape.simpleDescription())


class NamedShape {
    var numberOfSides: Int = 0
    var name: String
    
    init(name: String) {
        self.name = name
    }
    
    deinit {
        self.name = ""
    }
    
    
    func simpleDescription() -> String {
        return "A shape with name \(self.name) and \(numberOfSides) sides."
    }
    
}

let namedShape = NamedShape(name: "Nova")
print(namedShape.simpleDescription())



class Square: NamedShape {
    
    var sideLength: Double
    
    init(sideLength: Double, name: String) {
        self.sideLength = sideLength
        super.init(name: name)
        numberOfSides = 4
    }
    
    func area() -> Double {
        return sideLength * sideLength
    }
    
    override func simpleDescription() -> String {
        return "A Square with sides of length \(sideLength)"
    }
    
}


let square = Square(sideLength: 22.5, name: "Teste")
square.area()
square.simpleDescription()


class EquilateralTriangle: NamedShape {
    var sideLength = 0.0
    
    init(sideLength: Double, name: String) {
        self.sideLength = sideLength
        super.init(name: name)
        numberOfSides = 3
    }
    
    
    var perimeter: Double {
        get {
            return 3.0 * sideLength
        }
        
        set {
            sideLength = newValue / 3.0
        }
    }
    
    override func simpleDescription() -> String {
        return "An Equilateral Triangle with sides of length \(sideLength)"
    }
    
}

var triangle = EquilateralTriangle(sideLength: 6.78, name: "Triangulo teste")
print(triangle.perimeter)
triangle.perimeter = 20.9
print(triangle.sideLength)




// Optionals

let optionalSquare: Square? = Square(sideLength: 2.5, name: "optional square")
let sideLength = optionalSquare?.sideLength


// Enumerations

enum Rank: Int {
    case Ace = 1
    case Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten
    case Jack, Queen, King
    
    func simpleDescription() -> String {
        
        switch self {
        case .Ace:
            return "ace"
            
        case .Jack:
            return "jack"
            
        case .Queen:
            return "queen"
            
        case .King:
            return "king"
            
        default:
            return String(self.rawValue)
            
        }
        
    }
}


let ace = Rank.Ace
let aceRawValue = ace.rawValue

let card = Rank.Jack
print(card)


// Protocolos e extensões

protocol ExampleProtocol {
    var simpleDescription: String { get }
    
    mutating func adjust()
}


class SimpleClass: ExampleProtocol {
    
    var simpleDescription: String = "A very simple class"
    var anotherProperty: Int = 69105
    
    func adjust() {
        simpleDescription += " Now 100% adjusted."
    }
    
}


var a = SimpleClass()
a.adjust()
let aDescription = a.simpleDescription


struct SimpleStructure: ExampleProtocol {
    
    var simpleDescription: String = "A simple structure"
    
    mutating func adjust() {
        simpleDescription += " (adjusted)"
    }
}

var b = SimpleStructure()
b.adjust()
let bDescription = b.simpleDescription


// Extension


extension Int: ExampleProtocol {
    var simpleDescription: String {
        return "The number \(self)"
    }
    
    mutating func adjust() {
        self += 42
    }
}

var intTeste: Int = 7
print( intTeste.simpleDescription )
intTeste.adjust()
print( intTeste.simpleDescription )


let protocolValue: ExampleProtocol = a
print( protocolValue.simpleDescription )
//print( protocolValue.anotherProperty )  // Uncomment to see the error




// Generics

func repeatItem<Item>(item: Item, numberOfTimes: Int) -> [Item] {
    var result = [Item]()
    
    for _ in 0..<numberOfTimes {
        result.append(item)
    }
    
    return result
}

repeatItem("knock", numberOfTimes: 3)
repeatItem("Who's there?", numberOfTimes: 1)
repeatItem("No one", numberOfTimes: 1)



// Reimplement the Swift standard library's optional type
enum OptionalValue<Wrapped> {
    
    case None
    case Some(Wrapped)
    
}

var possibleInteger: OptionalValue<Int> = .None
possibleInteger = .Some(100)



// Using where

func anyCommonElements <T: SequenceType, U: SequenceType where T.Generator.Element: Equatable, T.Generator.Element == U.Generator.Element> (lhs: T, _ rhs: U) -> Bool {
    
    for lhsItem in lhs {
        for rhsItem in rhs {
            if lhsItem == rhsItem {
                return true
            }
        }
    }
    
    return false
}

anyCommonElements([1, 2, 3], [3])













